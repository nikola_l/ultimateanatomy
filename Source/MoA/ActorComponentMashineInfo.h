// Copyright by Nikola Lukic zlatnaspirala@gmail.com

#pragma once

#include "Components/ActorComponent.h"
#include "ActorComponentMashineInfo.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MOA_API UActorComponentMashineInfo : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActorComponentMashineInfo();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;


	UFUNCTION(BlueprintCallable, Category = "GETMAC_DESKTOP")
	static TArray < uint8 > GetMacAddress();

	UFUNCTION(BlueprintCallable , Category = "GETMAC_DESKTOP")
		static FString GetMacAddressString();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GETMAC_DESKTOP")
    FString GetMacAddressStringForMe;

	
};
